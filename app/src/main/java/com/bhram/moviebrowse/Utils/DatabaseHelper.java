package com.bhram.moviebrowse.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bhram on 11/6/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String NAME = "movie.db";
    private static final String TABLE_NAME = "wishlist_data";

    public static final String COL_MOVIE_ID = "movieId";
    public static final String COL_TITLE = "title";
    public static final String COL_POSTER = "poster";

    public DatabaseHelper(Context context) {
        super(context, NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " " +
                "( ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_MOVIE_ID + " TEXT, " +
                COL_TITLE + " TEXT, " +
                COL_POSTER + " TEXT) ";
        db.execSQL(createTable);
    }

    public Boolean insertMovie(String movieID, String title, String poster) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_MOVIE_ID, movieID);
        values.put(COL_TITLE, title);
        values.put(COL_POSTER, poster);

        long id = db.insert(TABLE_NAME, null, values);
        return id == 1;
    }

    public Boolean checkMovieExists(String movieId) {

        String query = "SELECT 1 FROM " + TABLE_NAME + " WHERE " + COL_MOVIE_ID + "=" + movieId;
        Cursor cursor = exeucQuery(query);
        boolean exist = cursor.getCount() > 0;
        cursor.close();
        return exist;
    }

    public Cursor getDataMovies() {
        String query = "SELECT * FROM " + TABLE_NAME;
        return exeucQuery(query);
    }

    public void deleteMovie(String movidID) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, COL_MOVIE_ID + "=" + movidID, null);
    }

    private Cursor exeucQuery(String sql) {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String deleteTable = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(deleteTable);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
