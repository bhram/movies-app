package com.bhram.moviebrowse.Enums;

/**
 * Created by Bhram on 11/3/2017.
 */

public enum TypeResultCall {
    Popular,
    Top_Rated,
    Discover,
    Search,
    Upcoming,
    Recommend,
    Similar
}
