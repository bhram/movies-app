package com.bhram.moviebrowse.Services;

import com.bhram.moviebrowse.Models.Casts;
import com.bhram.moviebrowse.Models.Details;
import com.bhram.moviebrowse.Models.Genres;
import com.bhram.moviebrowse.Models.MoviesResult;
import com.bhram.moviebrowse.Models.Reviews;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Bhram on 11/2/2017.
 */

public interface TMDBServices {
    String BASE_API = "https://api.themoviedb.org/3/";


    @GET("movie/{type}")
    Call<MoviesResult> ListMainMovies(
            @Path("type") String type,
            @QueryMap Map<String, String> params);


    @GET("genre/movie/list")
    Call<Genres> listGenres(
            @Query("api_key") String api
    );


    @GET("discover/movie")
    Call<MoviesResult> DiscoverMovies(
            @QueryMap Map<String, String> params);


    @GET("search/movie")
    Call<MoviesResult> SearchMovies(
            @QueryMap Map<String, String> params);


    @GET("movie/{id}/similar")
    Call<MoviesResult> SimilarMovie(
            @Path("id") String id, @QueryMap Map<String, String> params
    );

    @GET("movie/{id}/recommendations")
    Call<MoviesResult> RecommendMovie(
            @Path("id") String id, @QueryMap Map<String, String> params);

    @GET("movie/{id}")
    Call<Details> DetailsMovie(
            @Path("id") String id, @QueryMap Map<String, String> params
    );

    @GET("movie/{id}/casts?api_key=" + Api.API_KEY)
    Call<Casts> CastsMovie(@Path("id") String id);

    @GET("movie/{id}/reviews?api_key=" + Api.API_KEY)
    Call<Reviews> ReviewsMovie(@Path("id") String id);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_API)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


}
