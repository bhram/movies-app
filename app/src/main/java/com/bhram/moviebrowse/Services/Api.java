package com.bhram.moviebrowse.Services;

import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.Models.Casts;
import com.bhram.moviebrowse.Models.Genres;
import com.bhram.moviebrowse.Models.MoviesResult;
import com.bhram.moviebrowse.Models.Reviews;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

/**
 * Created by Bhram on 11/2/2017.
 */

public class Api {
    public static final String API_KEY = "e1a4c8306d04687c89a66263a4f13221";

    public static final String POPULAR_ROUTE = "popularity.desc";

    public static String URL_Image(String img) {
        return "https://image.tmdb.org/t/p/w500/" + img;
    }

    private static TMDBServices createTMDBService() {
        return TMDBServices.retrofit.create(TMDBServices.class);
    }

    public static Call<MoviesResult> BuildMovieResultCall(TypeResultCall type, int page, Map p) {
        if (type == null) return null;

        TMDBServices tmdbServices = createTMDBService();
        Map<String, String> params = new HashMap<>();
        params.put("api_key", API_KEY);
        params.put("page", String.valueOf(page));

        // if not empty then put all params
        if (p != null) {
            params.putAll(p);
        }
        switch (type) {
            case Popular:
            case Upcoming:
            case Top_Rated:
                return tmdbServices.ListMainMovies(
                        type.name().toLowerCase(),
                        params);
            case Discover:
                return tmdbServices.DiscoverMovies(params);
            case Search:
                return tmdbServices.SearchMovies(params);
            case Similar:
                return tmdbServices.SimilarMovie(params.get("id"), params);
            case Recommend:
                return tmdbServices.RecommendMovie(params.get("id"), params);
        }
        return null;
    }

    public static Call<Genres> BuildGenresCall() {
        return createTMDBService().listGenres(API_KEY);
    }

    public static Call<Casts> BuildCastsCall(String movieId) {
        return createTMDBService().CastsMovie(movieId);
    }

    public static Call<Reviews> BuildReviewsCall(String movieId) {
        return createTMDBService().ReviewsMovie(movieId);
    }
}
