package com.bhram.moviebrowse.Services;

import android.content.Context;
import android.content.DialogInterface;

import com.bhram.moviebrowse.Models.Genre;
import com.bhram.moviebrowse.Models.Genres;
import com.bhram.moviebrowse.Utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bhram on 11/4/2017.
 */

public class GenresService {

    public static Genres InstanceGenres;

    public static Genre getGenreFromName(String genre) {
        if (InstanceGenres != null) {
            for (Genre g : InstanceGenres.getGenres()) {
                if (g.getName().toLowerCase().equals(genre.toLowerCase())) {
                    return g;
                }
            }
        }
        return null;
    }

    public static Context context;

    private static void showAlert() {
        Utility.showAlertOnOffline(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                fetchGenres(context);
            }
        });
    }

    public static void fetchGenres(Context c) {
        if (!Utility.isInternetAvailable()) {
            context = c;
            showAlert();
        } else {
            Call<Genres> genreCall = Api.BuildGenresCall();
            genreCall.clone().enqueue(GenresCallback);
        }
    }

    private static Callback<Genres> GenresCallback = new Callback<Genres>() {
        @Override
        public void onResponse(Call<Genres> call, Response<Genres> response) {
            InstanceGenres = response.body();
        }

        @Override
        public void onFailure(Call<Genres> call, Throwable t) {
            showAlert();
        }
    };

}
