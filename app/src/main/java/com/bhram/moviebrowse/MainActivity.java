package com.bhram.moviebrowse;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.bhram.moviebrowse.Activity.SearchActivity;
import com.bhram.moviebrowse.Application.App;
import com.bhram.moviebrowse.Fragments.CategoriesFragment;
import com.bhram.moviebrowse.Fragments.FiltersFragment;
import com.bhram.moviebrowse.Fragments.MainFragment;
import com.bhram.moviebrowse.Fragments.WishFragment;
import com.bhram.moviebrowse.Services.GenresService;
import com.bhram.moviebrowse.Utils.Utility;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.include_toolbar)
    Toolbar toolbar;
    @BindView(R.id.adView)
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupToolbar();
        setupNavigation();
        setupAds();
        //Call to fetch data of genres
        GenresService.fetchGenres(this);
    }

    private void setupAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setupNavigation() {

        Utility.disableShiftMode(navigation);
        changeFragment(R.id.navigation_home);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                changeFragment(item.getItemId());
                return true;
            }
        });
    }


    private void changeFragment(int itemID) {

        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment newFragment = fragmentManager.findFragmentByTag(String.valueOf(itemID));
        if (newFragment == null) {
            if (itemID == R.id.navigation_home) {
                newFragment = new MainFragment();
            } else if (itemID == R.id.navigation_category) {
                newFragment = new CategoriesFragment();
            } else if (itemID == R.id.navigation_filters) {
                newFragment = new FiltersFragment();
            } else if (itemID == R.id.navigation_whishlist) {
                newFragment = new WishFragment();
            } else {
                return;
            }
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, newFragment, String.valueOf(itemID));
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activty_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            startActivity(new Intent(this, SearchActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        App.DB_Helper.close();
        super.onDestroy();
    }
}
