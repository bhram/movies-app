package com.bhram.moviebrowse.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.Fragments.BrowseMoviesFragment;
import com.bhram.moviebrowse.Models.Genre;
import com.bhram.moviebrowse.Services.Api;
import com.bhram.moviebrowse.Utils.Utility;

import java.util.HashMap;
import java.util.Map;

public class BrowseActivity extends AppCompatActivity {
    private static final int CONTENT_VIEW_ID = 10101010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout frame = new FrameLayout(this);

        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));


        Genre genre = (Genre) getIntent().getSerializableExtra("genre");
        seToolbarTitle(genre.getName());

        Map<String, String> params = new HashMap<>();
        params.put("sort_by", Api.POPULAR_ROUTE);
        params.put("with_genres", genre.getId());

        BrowseMoviesFragment browseMoviesFragment = new
                BrowseMoviesFragment(TypeResultCall.Discover, params);

        setFragment(browseMoviesFragment);//


    }

    private void seToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setFragment(BrowseMoviesFragment fragment) {
        Utility.addFragment(getSupportFragmentManager(), CONTENT_VIEW_ID, fragment);
    }

}
