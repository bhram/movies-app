package com.bhram.moviebrowse.Activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhram.moviebrowse.Adapter.ViewPagerAdapter;
import com.bhram.moviebrowse.Application.App;
import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.Fragments.BrowseMoviesFragment;
import com.bhram.moviebrowse.Fragments.CastsFragment;
import com.bhram.moviebrowse.Fragments.ReviewsFragment;
import com.bhram.moviebrowse.Models.Details;
import com.bhram.moviebrowse.Models.Genre;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Services.Api;
import com.bhram.moviebrowse.Services.TMDBServices;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ruslankishai.unmaterialtab.tabs.RoundTabLayout;
import com.squareup.picasso.Picasso;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity implements Callback<Details> {

    @BindView(R.id.movie_details_backdrop)
    ImageView backdrop;

    @BindView(R.id.img_poster)
    ImageView poster;

    @BindView(R.id.progressView)
    GeometricProgressView progressView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;


    Details movieDetails;
    @BindView(R.id.tv_years)
    TextView tvYears;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.tv_genres)
    TextView tvGenres;
    @BindView(R.id.tv_overview)
    TextView tvOverview;
    @BindView(R.id.round_tabs)
    RoundTabLayout roundTabs;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.img_fav)
    ImageButton imgFav;
    @BindView(R.id.adView)
    AdView adView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        App.incrementOpen++;

        setToolbar();
        setAds();
        String movieId = getIntent().getStringExtra("id");

        TMDBServices tmdbServices = TMDBServices.retrofit.create(TMDBServices.class);

        Map<String, String> params = new HashMap<>();
        params.put("api_key", Api.API_KEY);
        params.put("append_to_response", "videos");
        Call<Details> detailsCall = tmdbServices.DetailsMovie(movieId, params);

        detailsCall.clone().enqueue(this);
    }

    private void setAds() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResponse(Call<Details> call, Response<Details> response) {
        if (response.isSuccessful()) {
            movieDetails = response.body();
            setViews();
        }
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(Call<Details> call, Throwable t) {
        progressView.setVisibility(View.GONE);
    }


    private void setViews() {

        setFragments();

        imgFav.setSelected(App.DB_Helper.checkMovieExists(String.valueOf(movieDetails.getId())));
        imgFav.setVisibility(View.VISIBLE);

        Picasso.with(this)
                .load(Api.URL_Image(movieDetails.getBackdrop_path()))
                .fit().centerCrop()
                .into(backdrop);

        Picasso.with(this)
                .load(Api.URL_Image(movieDetails.getPoster_path()))
                .fit().centerCrop()
                .into(poster);

        tvTitle.setText(movieDetails.getTitle());

        tvRating.setVisibility(View.VISIBLE);
        tvRating.setText(String.valueOf(movieDetails.getVote_average()));

        tvOverview.setText(movieDetails.getOverview());
        tvYears.setText(movieDetails.getRelease_date().split("-")[0]);
        List<Genre> genres = movieDetails.getGenres();
        for (int i = 0; i < genres.size(); i++) {
            if (genres.size() - 1 != i) {
                tvGenres.append(genres.get(i).getName() + ", ");
            } else {
                tvGenres.append(genres.get(i).getName());
            }
        }
    }

    private void setFragments() {
        String id = String.valueOf(movieDetails.getId());
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Map<String, String> params = new HashMap<>();
        params.put("id", id);

        adapter.addFragment(new CastsFragment(id), "Casts");
        adapter.addFragment(new ReviewsFragment(id), "Reviews");
        adapter.addFragment(new BrowseMoviesFragment(TypeResultCall.Recommend, params), "Recommend");
        adapter.addFragment(new BrowseMoviesFragment(TypeResultCall.Similar, params), "Similar");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        roundTabs.setupWithViewPager(viewPager);
    }

    @OnClick(R.id.img_fav)
    public void setFav() {
        if (!App.DB_Helper.checkMovieExists(String.valueOf(movieDetails.getId()))) {
            App.DB_Helper.insertMovie(String.valueOf(movieDetails.getId()), movieDetails.getTitle(), movieDetails.getPoster_path());
            imgFav.setSelected(true);
        } else {
            App.DB_Helper.deleteMovie(String.valueOf(movieDetails.getId()));
            imgFav.setSelected(false);
        }
    }

}
