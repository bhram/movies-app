package com.bhram.moviebrowse.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.SearchView;

import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.Fragments.BrowseMoviesFragment;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Utils.Utility;

import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private static final int CONTENT_VIEW_ID = 20202020;
    SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frame = new FrameLayout(this);

        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));


        seToolbarTitle();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add("Search");
        item.setIcon(R.drawable.ic_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        sv = new SearchView(this);
        sv.setOnQueryTextListener(this);
        sv.setFocusable(true);
        sv.setIconified(false);
        sv.requestFocusFromTouch();
        item.setActionView(sv);
        return super.onCreateOptionsMenu(menu);

    }

    private void seToolbarTitle() {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        if (!TextUtils.isEmpty(s)) {
            search(s);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    private void search(String query) {
        Map<String, String> params = new HashMap<>();
        params.put("query", query);

        BrowseMoviesFragment browseMoviesFragment = new
                BrowseMoviesFragment(TypeResultCall.Search, params);

        Utility.addFragment(getSupportFragmentManager(), CONTENT_VIEW_ID, browseMoviesFragment);

    }
}
