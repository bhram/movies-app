package com.bhram.moviebrowse.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhram.moviebrowse.Adapter.ReviewsAdapter;
import com.bhram.moviebrowse.Models.Reviews;
import com.bhram.moviebrowse.Services.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bhram on 11/6/2017.
 */

@SuppressLint("ValidFragment")
public class ReviewsFragment extends Fragment implements Callback<Reviews> {
    String movieID;
    RecyclerView rv;

    public ReviewsFragment(String movieId) {
        this.movieID = movieId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = new RecyclerView(getContext());
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        fetchReviews();
        return rv;
    }

    private void fetchReviews() {
        Call<Reviews> reviewsCall = Api.BuildReviewsCall(movieID);
        reviewsCall.clone().enqueue(this);
    }

    @Override
    public void onResponse(Call<Reviews> call, Response<Reviews> response) {
        if (response.isSuccessful()) {
            setAdapter(response.body());
        }
    }

    @Override
    public void onFailure(Call<Reviews> call, Throwable t) {

    }

    private void setAdapter(Reviews review) {
        if (review.getResults().length > 0) {
            ReviewsAdapter reviewsAdapter = new ReviewsAdapter(getContext(), review);
            rv.setAdapter(reviewsAdapter);
        } else {

        }
    }
}
