package com.bhram.moviebrowse.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhram.moviebrowse.Activity.BrowseActivity;
import com.bhram.moviebrowse.Adapter.CategoriesAdapter;
import com.bhram.moviebrowse.Application.App;
import com.bhram.moviebrowse.Models.Genre;
import com.bhram.moviebrowse.Services.GenresService;
import com.bhram.moviebrowse.Utils.Utility;

import java.io.IOException;

/**
 * Created by Bhram on 11/4/2017.
 */

public class CategoriesFragment extends Fragment implements CategoriesAdapter.ItemClickListener {

    String[] listCategories;
    RecyclerView rv;

    public CategoriesFragment() {
        listCategories = getImagesCategoryInAssets();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = new RecyclerView(getContext());
        rv.setPadding(0, 0, 0, 150);
        rv.setClipToPadding(false);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));


        CategoriesAdapter categoriesAdapter = new CategoriesAdapter(getContext(), listCategories);
        categoriesAdapter.setClickListener(this);
        rv.setAdapter(categoriesAdapter);

        return rv;
    }

    public String[] getImagesCategoryInAssets() {
        try {
            String[] list = App.getInstance().getAssets().list("category");
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent i = new Intent(getActivity(), BrowseActivity.class);
        String name = Utility.getNameWithoutExtantion(listCategories[position]);
        Genre genre = GenresService.getGenreFromName(name);
        i.putExtra("genre", genre);
        startActivity(i);
    }
}
