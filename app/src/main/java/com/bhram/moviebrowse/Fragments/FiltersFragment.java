package com.bhram.moviebrowse.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.Models.Genre;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Services.GenresService;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Bhram on 11/4/2017.
 */

public class FiltersFragment extends Fragment {

    @BindView(R.id.toggle_layout)
    ViewGroup toggle_layout;

    @BindView(R.id.expandable_layout)
    ExpandableLayout expandable_layout;

    @BindView(R.id.ic_toggle_arrow)
    ImageView ic_toggle_arrow;

    @BindView(R.id.spinner_genres)
    Spinner spinner_genres;

    @BindView(R.id.spinner_sortBy)
    Spinner spinner_sortBy;

    @BindView(R.id.seek_rating)
    SeekBar seek_rating;

    @BindView(R.id.tv_rating)
    TextView tv_rating;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_filters, container, false);
        ButterKnife.bind(this, root);
        setupViews();
        return root;
    }

    private void setupViews() {

        if (GenresService.InstanceGenres != null) {
            List<Genre> genres = GenresService.InstanceGenres.getGenres();
            genres.add(0, new Genre("All"));
            ArrayAdapter<Genre> spinnerGenresAdapter = new ArrayAdapter(getContext(),
                    android.R.layout.simple_spinner_dropdown_item, genres);

            spinner_genres.setAdapter(spinnerGenresAdapter);


            ArrayAdapter<String> spinnerCountShoesArrayAdapter = new ArrayAdapter<String>
                    (getContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.sortByList));
            spinner_sortBy.setAdapter(spinnerCountShoesArrayAdapter);

            seek_rating.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    tv_rating.setText(String.valueOf(i) + "+");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }

            });

            toggle_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleExpandLayout();
                }
            });

            expandable_layout.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
                @Override
                public void onExpansionUpdate(float expansionFraction, int state) {
                    ic_toggle_arrow.setPressed(!expandable_layout.isExpanded());
                }
            });

        }

    }

    private void toggleExpandLayout() {
        if (expandable_layout.isExpanded()) {
            expandable_layout.collapse();
        } else {
            expandable_layout.expand();
        }
    }

    @OnClick(R.id.btn_filter)
    public void Filter(View view) {
        Genre genre = (Genre) spinner_genres.getSelectedItem();

        String id_gender = genre.getId();
        String sort_by = spinner_sortBy.getSelectedItem().toString().replace(" ", "_").toLowerCase() + ".desc";
        String rating = String.valueOf(seek_rating.getProgress());

        Map<String, String> params = new HashMap<>();
        params.put("sort_by", sort_by);
        params.put("with_genres", id_gender);
        params.put("vote_average.gte", rating);

        BrowseMoviesFragment browseMoviesFragment = new
                BrowseMoviesFragment(TypeResultCall.Discover, params);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainerFilter, browseMoviesFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        fragmentTransaction.commit();
    }
}
