package com.bhram.moviebrowse.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhram.moviebrowse.Adapter.ViewPagerAdapter;
import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Utils.Utility;
import com.ruslankishai.unmaterialtab.tabs.RoundTabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bhram on 11/1/2017.
 */

public class MainFragment extends Fragment {
    @BindView(R.id.round_tabs)
    RoundTabLayout roundTabLayout;

    @BindView(R.id.view_pager)
    ViewPager view_pager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, root);
        setViewAdapter();
        return root;
    }

    private void setViewAdapter() {
        if (!Utility.isInternetAvailable()) {
            Utility.showAlertOnOffline(getContext(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    setViewAdapter();
                }
            });
        } else {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
            adapter.addFragment(new BrowseMoviesFragment(TypeResultCall.Popular), "Popular");
            adapter.addFragment(new BrowseMoviesFragment(TypeResultCall.Top_Rated), "Top Rater");
            adapter.addFragment(new BrowseMoviesFragment(TypeResultCall.Upcoming), "Upcoming");
            view_pager.setAdapter(adapter);
            view_pager.setOffscreenPageLimit(3);
            roundTabLayout.setupWithViewPager(view_pager);
        }
    }
}
