package com.bhram.moviebrowse.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhram.moviebrowse.Adapter.CastsAdapter;
import com.bhram.moviebrowse.Models.Casts;
import com.bhram.moviebrowse.Services.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bhram on 11/5/2017.
 */

@SuppressLint("ValidFragment")
public class CastsFragment extends Fragment implements Callback<Casts> {
    String movieId;
    RecyclerView rv;

    public CastsFragment(String movieId) {
        this.movieId = movieId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = new RecyclerView(getContext());
        rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        fetchCasts();
        return rv;
    }

    private void fetchCasts() {
        Call<Casts> castsCall = Api.BuildCastsCall(movieId);
        castsCall.clone().enqueue(this);
    }

    @Override
    public void onResponse(Call<Casts> call, Response<Casts> response) {
        if (response.isSuccessful()) {
            setAdapter(response.body());
        }
    }

    @Override
    public void onFailure(Call<Casts> call, Throwable t) {

    }

    private void setAdapter(Casts casts) {
        CastsAdapter castsAdapter = new CastsAdapter(getContext(), casts.getCast());
        rv.setAdapter(castsAdapter);
    }

}
