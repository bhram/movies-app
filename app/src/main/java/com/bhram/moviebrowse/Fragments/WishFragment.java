package com.bhram.moviebrowse.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhram.moviebrowse.Activity.DetailsActivity;
import com.bhram.moviebrowse.Adapter.MoviesBrowserAdapter;
import com.bhram.moviebrowse.Adapter.WishAdapter;
import com.bhram.moviebrowse.Application.App;
import com.bhram.moviebrowse.Models.Results;
import com.bhram.moviebrowse.Utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bhram on 11/6/2017.
 */

public class WishFragment extends Fragment implements MoviesBrowserAdapter.ItemClickListener {
    RecyclerView rv;
    List<Results> results;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = new RecyclerView(getContext());

        GridLayoutManager gridLayoutManager =
                new GridLayoutManager(getContext(), Utility.calculateNoOfColumns());

        rv.setLayoutManager(gridLayoutManager);
        rv.setClipToPadding(false);
        rv.setPadding(0, 0, 0, 150);

        fetchMovie();
        return rv;
    }

    private void fetchMovie() {
        Cursor cursor = App.DB_Helper.getDataMovies();
        results = new ArrayList<>();
        while (cursor.moveToNext()) {
            String id = cursor.getString(1);
            String title = cursor.getString(2);
            String poster = cursor.getString(3);
            results.add(new Results(title, poster, id));
        }
        cursor.close();
        WishAdapter wishAdapter = new WishAdapter(getContext(), results);
        wishAdapter.setClickListener(this);
        rv.setAdapter(wishAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        Results movie = results.get(position);
        Intent i = new Intent(getActivity(), DetailsActivity.class);
        i.putExtra("id", movie.getId());
        startActivity(i);
    }
}
