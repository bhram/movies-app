package com.bhram.moviebrowse.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhram.moviebrowse.Activity.DetailsActivity;
import com.bhram.moviebrowse.Adapter.MoviesBrowserAdapter;
import com.bhram.moviebrowse.Enums.TypeResultCall;
import com.bhram.moviebrowse.Models.MoviesResult;
import com.bhram.moviebrowse.Models.Results;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Services.Api;
import com.bhram.moviebrowse.Utils.Utility;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BrowseMoviesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, MoviesBrowserAdapter.ItemClickListener {

    @BindView(R.id.recycler_grid)
    RecyclerView recyclerGrid;

    @BindView(R.id.progressView)
    GeometricProgressView progressView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    MoviesResult moviesResult;
    MoviesBrowserAdapter adapter;

    private TypeResultCall typeResultCall;
    private Map params;
    private int pageNumber = 1;

    public BrowseMoviesFragment() {
    }

    @SuppressLint("ValidFragment")
    public BrowseMoviesFragment(TypeResultCall typeResult, Map params) {
        this.typeResultCall = typeResult;
        this.params = params;
    }

    @SuppressLint("ValidFragment")
    public BrowseMoviesFragment(TypeResultCall typeResult) {
        this.typeResultCall = typeResult;
        this.params = null;
    }

    private Callback<MoviesResult> CallBackFetch = new Callback<MoviesResult>() {
        @Override
        public void onResponse(Call<MoviesResult> call, Response<MoviesResult> response) {
            if (response.isSuccessful()) {
                MoviesResult result = response.body();
                //check if not null and not list result not empty
                if (adapter != null & result.getResults().size() > 0) {
                    //if not first request
                    moviesResult.getResults().addAll(result.getResults());
                    adapter.notifyDataChanged();
                } else {
                    moviesResult = result;
                    setRecyclerView();
                }

                //increment page number
                pageNumber++;
            }
            //call method to hide loading view like progress and swipe refresh icon
            endRequest();
        }

        @Override
        public void onFailure(Call<MoviesResult> call, Throwable t) {
            endRequest();
        }
    };


    // method fetch next Page
    private void fetchData() {
        progressView.setVisibility(View.VISIBLE);
        Call<MoviesResult> moviesResultCall = Api.BuildMovieResultCall(typeResultCall, pageNumber, params);
        if (moviesResultCall != null) {
            moviesResultCall.clone().enqueue(CallBackFetch);
        } else {
            endRequest();
        }

    }

    //hide loading view like progress and swipe refresh icon
    private void endRequest() {
        progressView.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_browse_movie, container, false);
        ButterKnife.bind(this, root);
        fetchData();
        swipeRefresh.setOnRefreshListener(this);
        return root;
    }

    private void setRecyclerView() {
        GridLayoutManager gridLayoutManager =
                new GridLayoutManager(getContext(), Utility.calculateNoOfColumns());

        recyclerGrid.setLayoutManager(gridLayoutManager);
        adapter = new MoviesBrowserAdapter(moviesResult.getResults());
        recyclerGrid.setAdapter(adapter);
        adapter.setLoadMoreListener(new MoviesBrowserAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                fetchData();
            }
        });
        adapter.setClickListener(this);
    }

    @Override
    public void onRefresh() {
        fetchData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //Saving data while orientation changes
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onItemClick(View view, int position) {
        try {
            Results movie = moviesResult.getResults().get(position);
            Intent i = new Intent(getActivity(), DetailsActivity.class);
            i.putExtra("id", movie.getId());
            startActivity(i);
        } catch (Exception a) {

        }
    }
}
