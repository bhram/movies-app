package com.bhram.moviebrowse.Application;

import android.app.Application;

import com.bhram.moviebrowse.Utils.DatabaseHelper;

/**
 * Created by Bhram on 11/3/2017.
 */

public class App extends Application {
    private static App instance;
    public static DatabaseHelper DB_Helper;

    public static int incrementOpen;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        DB_Helper = new DatabaseHelper(this);
    }


    public static App getInstance() {
        return instance;
    }
}
