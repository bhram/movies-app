package com.bhram.moviebrowse.Models;

import java.io.Serializable;

/**
 * Created by Bhram on 11/5/2017.
 */

public class Genre implements Serializable {

    public Genre(String name) {
        this.name = name;
        this.id = "";
    }

    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
