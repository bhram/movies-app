package com.bhram.moviebrowse.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Bhram on 11/4/2017.
 */

public class Genres implements Serializable {

    List<Genre> genres;

    public List<Genre> getGenres() {
        return this.genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

}
