package com.bhram.moviebrowse.Models;

/**
 * Created by Bhram on 11/6/2017.
 */

public class Casts {
    private String id;

    private Cast[] cast;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cast[] getCast() {
        return cast;
    }

    public void setCast(Cast[] cast) {
        this.cast = cast;
    }


}
