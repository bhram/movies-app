package com.bhram.moviebrowse.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhram.moviebrowse.Models.Results;
import com.bhram.moviebrowse.Models.Reviews;
import com.bhram.moviebrowse.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bhram on 11/6/2017.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {
    Context mContext;
    LayoutInflater mInflater;
    Reviews reviews;

    public ReviewsAdapter(Context mContext, Reviews reviews) {
        this.mContext = mContext;
        this.reviews = reviews;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.review_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Results review = reviews.getResults()[position];
        holder.tvName.setText(review.getAuthor());
        holder.tvContent.setText(review.getContent());
    }

    @Override
    public int getItemCount() {
        return reviews.getResults().length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_content)
        TextView tvContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
