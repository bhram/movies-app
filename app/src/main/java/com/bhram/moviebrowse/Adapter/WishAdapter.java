package com.bhram.moviebrowse.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhram.moviebrowse.Models.Results;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Services.Api;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bhram on 11/6/2017.
 */

public class WishAdapter extends RecyclerView.Adapter<WishAdapter.ViewHolder> {
    Context mContext;
    LayoutInflater mInflater;
    List<Results> results;
    private MoviesBrowserAdapter.ItemClickListener mClickListener;

    public WishAdapter(Context mContext, List<Results> results) {
        this.mContext = mContext;
        this.results = results;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = mInflater.inflate(R.layout.grid_item, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Results movie = results.get(position);

        Picasso.with(mContext)
                .load(Api.URL_Image(movie.getPoster_path()))
                .into(holder.imgPoster, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.progressBar.setVisibility(View.GONE);
                    }
                });

        holder.tvTitle.setText(movie.getTitle());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    // allows clicks events to be caught
    public void setClickListener(MoviesBrowserAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_poster)
        ImageView imgPoster;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
