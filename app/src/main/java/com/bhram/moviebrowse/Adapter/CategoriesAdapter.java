package com.bhram.moviebrowse.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Utils.Utility;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bhram on 11/4/2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    Context mContext;
    LayoutInflater mInflater;
    String[] imagesList;

    private ItemClickListener mClickListener;

    public CategoriesAdapter(Context mContext, String[] imagesList) {
        this.mContext = mContext;
        this.imagesList = imagesList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.category_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String name = imagesList[position];
        holder.tv_genreName.setText(Utility.getNameWithoutExtantion(name));

        try {
            InputStream ims = mContext.getAssets().open("category/" + name);
            Drawable d = Drawable.createFromStream(ims, null);
            holder.img_category.setImageDrawable(d);
        } catch (IOException e) {
            Toast.makeText(mContext, "error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return imagesList.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_genreName)
        TextView tv_genreName;

        @BindView(R.id.img_category)
        ImageView img_category;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
