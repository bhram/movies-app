package com.bhram.moviebrowse.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhram.moviebrowse.Models.Cast;
import com.bhram.moviebrowse.R;
import com.bhram.moviebrowse.Services.Api;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Bhram on 11/6/2017.
 */

public class CastsAdapter extends RecyclerView.Adapter<CastsAdapter.ViewHolder> {
    Context mContext;
    LayoutInflater mInflater;
    Cast[] casts;


    public CastsAdapter(Context mContext, Cast[] casts) {
        this.mContext = mContext;
        this.casts = casts;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.cast_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Cast cast = casts[position];

        holder.tvName.setText(cast.getName());

        Picasso.with(mContext)
                .load(Api.URL_Image(cast.getProfile_path()))
                .into(holder.profileImage);
    }

    @Override
    public int getItemCount() {
        return casts.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.tv_name)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
